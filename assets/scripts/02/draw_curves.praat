# Plot overlapped f0 curves in different colours

form Plot overlapped F0 curves...
  sentence Path
  comment Leave empty for GUI selector
endform

if path$ == ""
  path$ = chooseDirectory$: "Select directory..."
  if path$ == ""
    exit
  endif
endif

files = Create Strings as file list: "files", path$ + "/*wav"
total_files = Get number of strings

for i to total_files

  selectObject: files
  file_name$ = Get string: i
  full_path$ = path$ + "/" + file_name$

  sound = Read from file: full_path$
  pitch = To Pitch: 0, 75, 150

  r = randomUniform(0.3, 1)
  g = randomUniform(0.3, 1)
  b = randomUniform(0.3, 1)
  Colour: "{'r','g','b'}"
  Draw: 0, 1, 60, 130, "yes"

  removeObject: sound, pitch
endfor

removeObject: files
