# Plot overlapped f0 curves in different colours

include ../../plugin_colour/procedures/rainbow.proc
include ../../plugin_utils/procedures/check_directory.proc
include ../../plugin_twopass/procedures/pitch_two-pass.proc

form Plot overlapped F0 curves...
  sentence Path
  comment Leave empty for GUI selector
endform

@checkDirectory: path$, "Choose directory..."
path$ = checkDirectory.name$

files = Create Strings as file list: "files", path$ + "/*wav"
total_files = Get number of strings

# For axes
minimum_time  = 1000
maximum_time  = 0
minimum_pitch = 1000
maximum_pitch = 0
padding       = 0.2

# For colors
saturation = 100
value      = 70

if total_files

  @rainbow: total_files, saturation, value
  for i to total_files
    selectObject: files
    file_name$ = Get string: i
    full_path$ = path$ + "/" + file_name$

    sound = Read from file: full_path$
    start = Get starting time
    end   = Get end time

    minimum_time = if start < minimum_time
      ... then start else minimum_time fi
    maximum_time = if end   > maximum_time
      ... then end else maximum_time fi

    @pitchTwoPass: 0.75, 1.5
    pitch[i] = selected("Pitch")

    minimum_pitch = if pitchTwoPass.floor < minimum_pitch
      ... then pitchTwoPass.floor else minimum_pitch fi
    maximum_pitch  = if pitchTwoPass.ceiling > maximum_pitch
      ... then pitchTwoPass.ceiling else maximum_pitch fi

    removeObject: sound
  endfor

  for i to total_files
    selectObject: pitch[i]
    Colour: rainbow.colours$[i]
    Draw:
      ... minimum_time  * (1 - padding),
      ... maximum_time  * (1 + padding),
      ... minimum_pitch * (1 - padding),
      ... maximum_pitch * (1 + padding),
      ... (i == 1)

    removeObject: pitch[i]
  endfor
endif

removeObject: files
