# Plot overlapped f0 curves in different colours

form Plot overlapped F0 curves...
  sentence Path
  comment Leave empty for GUI selector
  positive Minimum_pitch_(Hz) 75
  positive Maximum_pitch_(Hz) 150
endform

@gui_selector: path$
path$ = gui_selector.path$

files = Create Strings as file list: "files", path$ + "/*wav"
total_files = Get number of strings

# For time axis
minimum_time = 1000
maximum_time = 0

# For axes padding
padding = 0.2

if total_files
  for i to total_files
    selectObject: files
    file_name$ = Get string: i
    full_path$ = path$ + "/" + file_name$

    sound = Read from file: full_path$
    start = Get starting time
    end   = Get end time

    if start < minimum_time
      minimum_time = start
    endif

    if end > maximum_time
      maximum_time = end
    endif

    pitch[i] = To Pitch: 0, minimum_pitch, maximum_pitch

    removeObject: sound
  endfor

  for i to total_files
    selectObject: pitch[i]
    @random_color()
    Draw:
      ... minimum_time  * (1 - padding),
      ... maximum_time  * (1 + padding),
      ... minimum_pitch * (1 - padding),
      ... maximum_pitch * (1 + padding),
      ... "yes"

    removeObject: pitch[i]
  endfor
endif

removeObject: files

procedure random_color()
  .minimum_color = 0.3
  .r = randomUniform(.minimum_color, 1)
  .g = randomUniform(.minimum_color, 1)
  .b = randomUniform(.minimum_color, 1)
  Colour: "{'.r','.g','.b'}"
endproc

procedure gui_selector: .path$
  if .path$ == ""
    .path$ = chooseDirectory$: "Select directory..."
    if .path$ == ""
      exit
    endif
  endif
endproc
