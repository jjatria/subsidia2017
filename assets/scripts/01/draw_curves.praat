# Plot overlapped f0 curves in different colours

form Plot overlapped F0 curves...
  sentence Path /home/baboso/desktop
endform

Erase all
files = Create Strings as file list: "files", path$ + "/*wav"
total_files = Get number of strings

for i to total_files
  selectObject: files
  file_name$ = Get string: i
  full_path$ = path$ + "/" + file_name$

  Read from file: full_path$
  To Pitch: 0, 75, 150

  # Generate a random color
  r = randomUniform(0.3, 1)
  g = randomUniform(0.3, 1)
  b = randomUniform(0.3, 1)
  Colour: "{'r','g','b'}"

  # Plot
  Draw: 0, 1, 60, 130, "yes"
endfor

select all
Remove
