form Generate sample sentences...
  sentence Path sentences.txt
  word Language English
  word Voice default
endform

sentences = Read Strings from raw text file: path$
n = Get number of strings
synth = Create SpeechSynthesizer: language$, voice$

pad$ = "00000"

for i to n
  selectObject: synth
  sound = To Sound: Object_'sentences'$[i], "no"
  Save as WAV file: right$(pad$ + string$(i), 3) + ".wav"
  Remove
endfor

removeObject: synth, sentences
