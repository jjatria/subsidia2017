<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>An introduction to CPrAN</title>

    <link rel="stylesheet" href="../css/reveal.css">
    <link rel="stylesheet" href="../css/theme/beige.css">
    <link rel="stylesheet" href="../css/theme/jjatria.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="../lib/css/zenburn.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? '../css/print/pdf.css' : '../css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">

        <section data-markdown>
          <script type='text/template'>
            ## An introduction to CPrAN

            ##### José Joaquín Atria

            @jjatria

            [www.pinguinorodriguez.cl](http://www.pinguinorodriguez.cl)

            ---

            21 June, 2017 — Subsidia
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## About me

            * Using Praat since 2007

            * Used it both commercially and for research

            * Partially responsible for moving development to Github

            * Active on Praat user's list

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Collophon

            * Slides available at

              [jjatria.gitlab.io/subsidia2017/cpran-intro](https://jjatria.gitlab.io/subsidia2017/cpran-intro)

            * Source code available at

              [gitlab.com/jjatria/subsidia2017](https://gitlab.com/jjatria/subsidia2017)

            * Demo snapshot of CPrAN plugins available at

              [jjatria.gitlab.io/subsidia2017/assets/snapshot.zip](https://jjatria.gitlab.io/subsidia2017/assets/snapshot.zip)

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Overview

            * What is CPrAN?

            * Why do we need it?

            * How does it work?

            * How can _I_ use CPrAN?

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## What is CPrAN?

            The ["Comprehensive Praat Archive Network"](http://cpran.net)

            * A centralised repository for Praat tools and materials

            * A set of development guidelines

            * A protocol for the distribution of new materials

            * A client to make its use easier

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## Why do we need it?
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### Issues in the Praat community

            * Do you think it has any problems?

            * Can you think of solutions?

            <aside class="notes">
              How many of those present have heard of CPrAN?
              How many have used it?
              How many have written their own scripts? What about plugins?
              What problems have they come across?
              Take this time to learn about the attendees and where they come
              from.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### A developer's take on the issues

            * Lack of a unified community of developers

            * Large amount of repeated work

            * Increasing number of unmaintained projects

            * Everybody loses

            <aside class="notes">
              Praat has existed for a long time, but it doesn't have a unified
              community of developers, which means most people work on their
              own.

              This in turn means that a lot of the projects out there are
              written to solve problems that are very specific, and often times
              very common. This results in a lot of scripts that do the same
              thing, which means that there's repeated efforts.

              And because people work on scripts to solve specific problems,
              they end up being subject to "feature-creep", or being left
              unmaintained, when the project that spawned them ends, or the
              researcher moves to a different, also specific problem.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### This is not a new problem

            * This problem affects many tools and communities

            * How do they solve it?

            <aside class="notes">
              Even those who have never used or even heard about CPrAN must have
              used or heard about other packages and modules.
              If they use LaTeX, they've probably used CTAN.
              If they use R, they've probably used CRAN.
              If they use Perl, they've probably used CPAN.
              If they use Python, they've probably used pip.
              If they use Ruby, they've probably used gems.

              Why are these things not available in Praat?
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### How have other communities solve this?

            * By bringing the community together

              * Lists and forums

              * Conferences and meetups

            * By providing tools for developers to collaborate

              * Global directories

              * Package managers

              * Support for modules and dependencies

              * Open development

            <aside class="notes">
              The problem deep down is the generation of a community of users,
              not just developers. The problem is not a technical problem, but
              a human one.

              The idea is not just to create the tools to collaborate, but also
              to establish environments where those collaborations can come out.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## What is CPrAN?

            Mainly, it is a tool _to help establish a community_

            <aside class="notes">
              CPrAN is an open-ended project, whose main objective is _not_ to
              create a package manager. The long term goal is to help create a
              community.

              Under this umbrella, there are many different components,
              including the website itself, the client, etc. But also the
              creation of guidelines, and eventually the modification of the
              Praat codebase itself.

              Everybody is invited to collaborate in any way.
            </aside>
          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## How does it work?
          </script>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### What is needed for CPrAN to work

              * **A "package" to bundle code: a _plugin_**

              * The possibility to extend other packages

              * A central directory

              * A client

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### What is a plugin?

              From the [documentation](http://www.fon.hum.uva.nl/praat/manual/plug-ins.html):

              <blockquote class="left-aligned">
              When Praat starts up, it will execute all Praat scripts called
              `setup.praat` that reside in directories whose name starts with
              `plugin_` and that themselves reside in your Praat preferences
              directory.
              </blockquote>

              * Anything inside that directory is _up to us_

              <aside class="notes">
              The definition of a plugin is intentionally vague, which means we
              are free to specify what the referenced directory will hold.

              The content (and the existence) of the `setup.praat` script is also
              up to us.
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### What does a plugin give us?

              * A project-specific directory structure

              * Nested under the Praat directory tree

              * The _possibility_ of a startup script

              * We are free to determine _everything_ that goes into one

              <aside class="notes">
              </aside>
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### What is needed for CPrAN to work

              * A "package" to bundle code: a _plugin_

              * **The possibility to extend other packages**

              * A central directory

              * A client

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### Plugins as building blocks

              * Promotes collaboration

              * Promotes smaller / easier to maintain plugins

              * Makes it possible to do more with less

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### Does Praat support this?

              * `include` inserts code into your scope

              * `runScript()` calls external scripts

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `include`

              ``` perl
              # Contents of "some/script.proc"
              procedure add: .a, .b
                .sum = .a + .b
              endproc
              ```

              ``` perl
              # Some other script
              include some/script.proc
              @add: 3, 6
              appendInfoLine: "The sum is ", add.sum
              ```

              * The contents of another file are inserted into yours

              * Useful to reduce repeated code

              * Scope _is_ shared

              * Cannot interpolate paths!

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `runScript`

              ``` perl
              # Contents of "sample_sound.praat"
              synth = Create SpeechSynthesizer: "English", "default"
              To Sound: "This is some text", "no"
              removeObjcet: synth
              ```

              ``` perl
              # Some other script
              runScript: "sample_sound.praat"
              appendInfoLine: "I created a sample Sound!"
              ```

              * Call another script from yours

              * Path can be interpolated

              * Scope _is not_ shared

              <aside class="notes">
              </aside>
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### What is needed for CPrAN to work

              * A "package" to bundle code: a _plugin_

              * The possibility to extend other packages

              * **A central directory**

              * A client

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### The current state of affairs

              * There is currently no single directory for Praat code

              * There are individual efforts, often with individual scripts

              * But these are themselves hard to find!

              * Most code exchange is done hand-to-hand

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### The CPrAN website

              * [cpran.net](http://cpran.net) holds this catalogue

              * Current version is static (not automatically generated)

              * Downloadable archives and documentation

              * Linked to [gitlab.com](https://gitlab.com) to track development

              * Currently in development. Assistance welcome!

              <aside class="notes">
              </aside>
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### What is needed for CPrAN to work

              * A "package" to bundle code: a _plugin_

              * The possibility to extend other packages

              * A central directory

              * **A client**

              <aside class="notes">
              </aside>
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### The CPrAN client

              * Current client uses Perl

              * Install with `cpanm`:

                `cpanm https://gitlab.com/cpran/cpran.git`

              * Also available as a Docker image

                `docker run -it jjatria/cpran /bin/bash`

              <aside class="notes">
              </aside>
            </script>
          </section>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### The general architecture

            * Plugins are hosted on [gitlab.com](https://gitlab.com/cpran)

            * Remote database automatically kept up to date

            * Local database downloaded by client

            * Client uses local database to handle requests

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ## How can I use CPrAN?
          </script>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ## Get the client

              <video autoplay loop>
                <source src="../assets/demo.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Makes it _much_ easier to use
              * Curently in development, but definitely usable
              * Streamlining will follow once interface is complete
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ## Perl

              ``` bash
              cpanm https://gitlab.com/cpran/cpran.git
              cpran list
              ```

              * Will install client locally
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ## Docker

              ``` bash
              docker run -it --name cpran --detach jjatria/cpran /bin/bash
              docker exec cpran cpran <command> [options] [arguments]
              ```

              * Runs client within a container
              * Changes can be made permanent with volumes
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ## Or use git and the website

              ``` bash
              cd ~/.praat-dir
              git clone "https://gitlab.com/cpran/plugin_$name"
              # Repeat with all dependencies, and their dependencies
              ```

              * Time consuming and more difficult, but possible!
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ## Using the client

              * Only command line interface (for now!)
              * The client links to the installed version of Praat
              * Preferences directory and binary are customisable

              ``` bash
              cpran <command> [options] [arguments]
              ```
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ## Client commands
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `init`

              Initialise a CPrAN installation

              Run as the first command after installation!

            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `update`

              Update the local database

              ``` html
              $ cpran update
              Updating plugin data...
              Updated 19 packages
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `list`

              Lists all registered plugins

              ``` html
              $ cpran list
              Name          Local  Remote Description
              config        0.0.2  0.0.4  Read a config file
              htklabel      0.0.1         Read and write HTK/HTS label files
              json          0.0.1  0.0.1  read and write valid JSON using Praat
              momel-intsint             . [Not a CPrAN plugin]
              ...
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `search`

              Searches for specified plugins

              ``` html
              $ cpran search json
              Name      Local  Remote Description
              json      0.0.1  0.0.1  read and write valid JSON using Praat
              serialise 0.0.12 0.0.12 read/write JSON or YAML Praat objects
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `show`

              Shows the metadata for a given plugin

              * Shows description, requirements, author details, etc
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `install`

              Install and test plugins and their dependencies

              ``` html
              $ cpran install vieweach
              The following plugin will be INSTALLED:
                utils selection strutils vieweach
              Do you want to continue? [Y/n] y
              Contacting server...
              Querying repository URL...
              ...
              utils installed successfully.
              ...
              selection installed successfully.
              ...
              strutils installed successfully.
              ...
              vieweach installed successfully.
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `upgrade`

              Install and test plugins and their dependencies

              ``` html
              $ cpran upgrade
              The following plugin will be UPGRADED:
                strutils
              Do you want to continue? [Y/n]
              Upgrading strutils from v0.0.1 to v0.0.16...
              ...
              strutils upgraded successfully
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### `remove`

              Remove a plugin from disk

              ``` html
              $ cpran remove strutils
              The following plugin will be REMOVED:
                strutils
              Do you want to continue? [Y/n]
              Removing strutils...
              ```
            </script>
          </section>

          <section data-markdown>
            <script type='text/template'>
              ### Other commands

              * There are many other commands!

              * Check them out with `cpran commands`

            </script>
          </section>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### CPrAN highlights
          </script>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### [`twopass`](http://cpran.net/docs/plugins/twopass)

              <video autoplay loop>
                <source src="../assets/twopass.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Implements DeLooze and Hirst's two-pass algorithm
              * Per-utterance estimation of pitch thresholds
              * Allows both single and batch processing
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### [`selection`](http://cpran.net/docs/plugins/selection)

              <video autoplay loop>
                <source src="../assets/selection.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Save, restore, and manage Praat selections
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### [`tgutils`](http://cpran.net/docs/plugins/tgutils)

              <video autoplay loop>
                <source src="../assets/tgutils.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Multiple utilities for TextGrids
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### [`htklabel`](http://cpran.net/docs/plugins/htklabel)

              <video autoplay loop>
                <source src="../assets/htklabel.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Read and write HTK label files
              * Supports alternatives, levels, and MLF files
            </script>
          </section>
        </section>

        <section>
          <section data-markdown>
            <script type='text/template'>
              ### [`vieweach`](http://cpran.net/docs/plugins/selection)

              <video autoplay loop>
                <source src="../assets/vieweach.mp4" type="video/mp4" />
                [ Video not supported ]
              </video>

              * Makes it easier to inspect series of files
              * Opens an editor for each object or set of objects
              * Good example of how powerful things can become
            </script>
          </section>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### What now?

            * Get involved and contribute!

            * Join the [mailing list](https://groups.google.com/forum/#!forum/cpran)!

            * Come to [my second workshop](../cpran-releases) tomorrow

          </script>
        </section>

        <section data-markdown>
          <script type='text/template'>
            ### Questions?

            ( I'm available to help set things up )

            Thank you for listening!
          </script>
        </section>

      </div>
    </div>

    <script src="../lib/js/head.min.js"></script>
    <script src="../js/reveal.js"></script>

    <script>
      // More info https://github.com/hakimel/reveal.js#configuration
      Reveal.initialize({
        history: true,
        // Bounds for smallest/largest possible scale to apply to content
        minScale: 0.2,
        maxScale: 1.0,
        showNotes: false,

        // More info https://github.com/hakimel/reveal.js#dependencies
        dependencies: [
          { src: '../plugin/markdown/marked.js' },
          { src: '../plugin/markdown/markdown.js' },
          { src: '../plugin/notes/notes.js', async: true },
          { src: '../plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
        ]
      });
    </script>
  </body>
</html>
